
-- SUMMARY --

This module allows admin to block all user from a particular role to get login
into the site.It is also compatible with logintoboggon, email_registration and
two_steps_login, so you will not face any conflict with these modules.

-- REQUIREMENTS --
  No

-- INSTALLATION --
* Install as usual, see http://drupal.org/node/895232 for further information.

-- CONFIGURATION --
* Configure user permissions in Administration » People » Permissions:


-- CUSTOMIZATION --
* Access menu from here : admin/config/people/role-block

-- CONTACT --
Current maintainers:
* Ankush Gautam https://www.drupal.org/u/agautam
  email : ankushgautam76@gmail.com
* Sumit Prajapati https://www.drupal.org/u/sprajapati
  email : prajapatisumit.sw@gmail.com

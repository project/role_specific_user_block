<?php

/**
 * @file
 * Admin settings form and callbacks.
 */

/**
 * Implements hook_settings_form().
 */
function role_specific_user_block_settings_form() {
  $user_array = array_diff(user_roles(), array('administrator', 'anonymous user'));
  foreach ($user_array as $key => $value) {
    $roles[$key] = ucwords($value);
  }
  $form['role_specific_user_block'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select Role'),
    '#description' => t('Choosing Role will block all user related to this role.'),
    '#options' => $roles,
    '#default_value' => variable_get('role_specific_user_block', array()),
  );
  $form['role_specific_user_block_err_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Message'),
    '#description' => t('Enter error message to show when user login.'),
    '#required' => TRUE,
    '#default_value' => variable_get('role_specific_user_block_err_message'),
  );
  return system_settings_form($form);
}
